---

Create a Discord bot that uses OpenAI to generate responses to messages using Python
This article intends to explain how to create a simple Discord bot that replies as OpenAI's chat would do. And gives you a walkthrough of containerizing the application.

---

**What do you need?**

You need an OpenAI account api-key which you can find in your OpenAI account.

You need a configured Discord bot, here is how to do that.

Make sure your Discord bot has permissions to read and to send messages, and it's added to your discord channel.

---

**First things first** let's use discord's `Client` class to create the bot, and install this library with command: 

    pip install discord.py


We'll first create our own class named OpenAIBot which will inherit the `Client` class and add two more functions to it. This `Client` class takes intents as a constructor argument on initialization.


You could use the default `Intents` unless you have a particular one to specify.

    import openai
    from discord import Client, Intents

    class OpenAIBot(Client):
        def __init__(self):
            super().__init__(intents=Intents.all())

Let's override the asynchronous function `on_message`, which is triggered when a message is sent to the channel you added your discord bot to.

    import os
    import openai

    from discord import Client, Intents

    class OpenAIBot(Client):
        def __init__(self):
            super().__init__(intents=Intents.all())

        async def on_message(self, message):
            # this condition prevents the bot to reply to itself
            if message.author == self.user:
                return
            response = "Hello back" # <- this will change in the future
            await message.channel.send(response)


Now we have to deal with OpenAI's library for python, first lets install it:

    pip install --upgrade openai

The only thing we need for the OpenAI to be able to respond to our messages is the `Completion` engine and that is possible with a simple request:

    curl https://api.openai.com/v1/completions \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer YOUR_API_KEY' \
    -d '{
    "model": "text-davinci-003",
    "prompt": "Say this is a test",
    "max_tokens": 7,
    "temperature": 0
    }'

For more information on customizing this call check out the API reference of OpenAI.

Back to our code, we will now add a function called openai_response which takes a message as an argument and returns a message from OpenAPI.

We will use `openai.Completion.create()` function to help us out with the communication:

    class OpenAIBot(Client):
        def __init__(self):
            super().__init__(intents=Intents.all())

        async def on_message(self, message):
            if message.author == self.user:
                return
            response = self.openai_response(message.content)
            await message.channel.send(response)

        def openai_response(self, message: str) -> str:
            openai.api_key = "<OPENAI_TOKEN>"
            response = openai.Completion.create(
                model="text-davinci-003",
                prompt=message,
                temperature=0,
                max_tokens=2000,
                top_p=1,
                frequency_penalty=0,
                presence_penalty=0,
            )
            if response.get("choices"):
                return response.get("choices")[0]["text"]

Great!

Now running the bot would require the initialization of this class and the call of run of the super-class `Client`.

We'll use the condition `if __name__=="__main":` to avoid starting the bot when we (if we) import OpenAIBot in the near future.

Now the code looks like this:

    import os
    import openai
    from discord import Client, Intents

    class OpenAIBot(Client):
        def __init__(self):
            super().__init__(intents=Intents.all())

        async def on_message(self, message):
            if message.author == self.user:
                return
            response = self.openai_response(message.content)
            await message.channel.send(response)

        def openai_response(self, message: str) -> str:
            openai.api_key = "<OPENAI_API_KEY">
            response = openai.Completion.create(
                model="text-davinci-003",
                prompt=message,
                temperature=0,
                max_tokens=2000,
                top_p=1,
                frequency_penalty=0,
                presence_penalty=0,
            )
            if response.get("choices"):
                return response.get("choices")[0]["text"]

    if __name__ == "__main__":
        bot = OpenAIBot()
        bot.run("<DISCORD_API_KEY>")

There is a problem here, it's not good to directly insert confidential data like API keys on your code. We will use os.getenv to load these constants from our system.

After some minor tweaks the code will look like this:

    import os
    import openai
    from discord import Client, Intents

    class OpenAIBot(Client):
        def __init__(self):
            self.discord_bot_token = os.getenv("DISCORD_API_KEY")
            self.openai_api_key = os.getenv("OPENAI_API_KEY")
            super().__init__(intents=Intents.all())
            self.run_bot()

        async def on_message(self, message):
            if message.author == self.user:
                return
            response = self.openai_response(message.content)
            await message.channel.send(response)

        def openai_response(self, message: str) -> str:
            openai.api_key = self.openai_api_key
            response = openai.Completion.create(
                model="text-davinci-003",
                prompt=message,
                temperature=0,
                max_tokens=2000,
                top_p=1,
                frequency_penalty=0,
                presence_penalty=0,
            )
            if response.get("choices"):
                return response.get("choices")[0]["text"]

        def run_bot(self):
            super().run(self.discord_bot_token)

    if __name__ == "__main__":
        bot = OpenAIBot()

Assuming our main python script is named main.pywe can proceed to execute it:

`python3 main.py `

Now you will see indications that our discord bot is running if you configured everything correctly.

**How about using docker?**

I love Docker, so yes…

Let's create `Dockerfile` and `docker-compose.yml` for our docker configuration then `.env` for our environment variables, our project structure now looks like this:

    open-discord/
        bot/
            main.py
            requirements.txt
        .env
        docker-compose.yml
        Dockerfile

Here we can explain the content and functionality of our new files, `.env` will hold the Discord and OpenAI tokens.

    OPENAI_API_KEY=""
    DISCORD_API_KEY=""

A new file in our project is requirements.txt as well, you can generate by freezing dependencies to this file with command:

`pip freeze -> requirements.txt` 

Avoiding getting extra dependencies written in requirements here is the file I generated using virtual environment:

    aiohttp==3.8.3
    aiosignal==1.3.1
    async-timeout==4.0.2
    attrs==22.1.0
    certifi==2022.9.24
    charset-normalizer==2.1.1
    discord.py==2.1.0
    et-xmlfile==1.1.0
    frozenlist==1.3.3
    idna==3.4
    multidict==6.0.3
    numpy==1.23.5
    openai==0.25.0
    openpyxl==3.0.10
    pandas==1.5.2
    pandas-stubs==1.5.2.221124
    python-dateutil==2.8.2
    pytz==2022.6
    requests==2.28.1
    six==1.16.0
    tqdm==4.64.1
    types-pytz==2022.6.0.1
    typing_extensions==4.4.0
    urllib3==1.26.13
    yarl==1.8.2

Now let's explain Dockerfile, first we'll use `python:3.8-slim-buster` as the base image to build our container on, then set `/bot` as our working directory, copy bot directory from project root to container and install the required dependencies:

    FROM python:3.8-slim-buster  # base image

    WORKDIR /bot  # make /bot the working directory

    COPY ./bot /bot  # copy bot directory from project root

    RUN pip install --upgrade pip  # upgrade container's pip
    RUN pip install -r requirements.txt  # install dependencies

Simple `docker-compose.yml` configuration with a single container which is built based on the instructions on `Dockerfile`:

    version: "3"

    services:
    app:
        build: .
        env_file: .env
        command: python main.py

Build the docker image and spin up the container:

`docker-compose up --build -d`

And now you have the OpenAI discord bot running in a container.
