# openai-discord

<br>

### Description

<br>

A script to add OpenAI as a bot to your Discord channel.

OpenAI is a conversational AI system that listens, learns, and challenges.

This script lets those conversations find place in Discord.

<br>

#### What do you need?

<br>

You need OpenAI account and which you can find in [your account openai's api-keys](https://beta.openai.com/account/api-keys).

<br>

You need a configured Discord bot, [here is how to do that](https://www.ionos.co.uk/digitalguide/server/know-how/creating-discord-bot/).

Make sure your Discord bot has permissions to read and to send messages, and its added to your discord channel.


<br>

---

<br>

### Setup


<br>


#### **Setting up with docker:**

<br>


*Based on `.env.example` create a new `.env` file in base directory of the project.
And write write your own keys/tokens.*


- `docker-compse up --build -d`


<br>


<br>


#### **Setting up with python:**

<br>

*Make sure to add variables at `.env.example` in yout system environments*


- Create virtual environment:
`python3 -m virtualenv venv`


- Activate virtual environment:
`source venv/bin/activate`


- Move to bot directory:
`cd bot`


- Install dependences:
`pip install -r requirements.txt`


- Run bot:
`python main.py`


<br>

<br>

---

<br>

Enjoy!

<br>
